//----- рандом 1...100
function getOneToHundred() {
    let rand = Math.random() * 100;
    return Math.ceil(rand);
}

function getRandomArray() {
    let array = [];
    for(let i=0; i<10; i++)
    {
        let number = getOneToHundred();
        array.push(number);
    }
    return array;
}

let randomArray = getRandomArray();
console.log(randomArray);

//----- Среднее геометрическое

function getAvgGeometric(array) {
    let geometric = 1;
    array.forEach(element => geometric *= element);
    let avgGeometric = Math.pow(geometric, 1/array.length);
    return avgGeometric;
}
let avgGeometric = getAvgGeometric(randomArray);
console.log(avgGeometric);

//----------------------------

const NAMES = ['Андрей', 'Алексей', 'Александр', 'Борис', 'Виталий', 'Виктор', 'Марина', 'Светлана', 'Ева', 'Мая', 'Ангелина', 'Екатерина',
'Сергей', 'Дмитрий', 'Надежда', 'Михаил', 'Петр', 'Игорь', 'Ирина', 'Елена', 'Анастасия', 'Полина', 'Валентина', 'Анна', 'Юлия', 'Галина',
'Иван', 'Леонид', 'Евгений', 'Николай'];

function randomIndex(min, max){
    let index = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(index);
}

let users = [];
function generateRandomUsers() {
    for(let i = 0; i<10; i++)
    {
        let index = randomIndex (0, NAMES.length - 1);
        let user = {
            id: i + 1,
            name: NAMES[index],
            age: randomIndex (15, 80)
        };
        users.push(user);
    }
}
generateRandomUsers();
console.log(users);

//------- % пользователей старше 50 лет

function getUsersOlder50() {
    let n = 0;
    for(let i = 0; i<users.length; i++)
    {
        if(users[i].age>50)
        {
            n++;
        }
    }
    let usersOlder50 = n*100/users.length;
    return usersOlder50 + ' %';
}

let usersOlder50 = getUsersOlder50();
console.log(usersOlder50);

//-------- Сортировка по имени

/*users.sort((a, b) => a.name.localeCompare(b.name));
console.log(users);*/

//-------- Массив имен

let newNames = [];

function getNames() {
    for(let i = 0; i<users.length; i++)
    {
        newNames.push(users[i].name);
    }
    return newNames;
}

newNames = getNames();
console.log(newNames);

//-------- Удаление пользователя

let newUsers = [];

function delUser() {
    let spUsers = users.filter(item => item.id < 3 || item.id > 3);
    return spUsers;
}
newUsers = delUser();
console.log(newUsers);

//---------- Добавление пользователя

function addingUser() {
    let index = randomIndex (0, NAMES.length - 1);
    let userPlus = {
        id: users.length + 1,
        name: NAMES[index],
        age: randomIndex (15, 80)
    };
    users.push(userPlus);
    return users;
}

addingUser();
console.log(users);
